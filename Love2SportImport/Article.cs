﻿using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Love2SportImport
{
    public class Article
    {

        [CsvColumn(Name = "artikel", FieldIndex = 1)]
        public String SKU { get; set; }


        [CsvColumn(Name = "maat", FieldIndex = 2)]
        public String Maat { get; set; }
    }
}
