﻿using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Love2SportImport
{
    public class Program
    {
        static void Main(string[] args)
        {

            CsvFileDescription inputFileDescription = new CsvFileDescription
            {
                SeparatorChar = '|',
                FirstLineHasColumnNames = true,
                IgnoreUnknownColumns = true,
                //EnforceCsvColumnAttribute = true
                //FileCultureName = "nl"
            };

            CsvContext cc = new CsvContext();
            Store_Love2SportEntities dbContext = new Store_Love2SportEntities();
            dbContext.Configuration.AutoDetectChangesEnabled = false;

            IEnumerable<Article> products =
    cc.Read<Article>("81772-66200411-forecast.csv", inputFileDescription);


            //This attribute is the global product attribue that is used
            int maatAttributeId = 36;

            var articles = products.GroupBy(e => e.SKU).Count();
            int count = 0;

            foreach (var item in products.GroupBy(e => e.SKU))
            {
                var dbProduct = dbContext.Products.Where(e => e.Deleted == false && e.Sku == item.Key).FirstOrDefault();

                if (dbProduct == null)
                    continue;

                if (item.Any(e => !String.IsNullOrEmpty(e.Maat)))
                {
                    //Insert a mapping for the product. There is only one mapping row needed per product/productattribute coupling.
                    var mapping = dbContext.Product_ProductAttribute_Mapping.Add(new Product_ProductAttribute_Mapping()
                    {
                        ProductId = dbProduct.Id,
                        ProductAttributeId = maatAttributeId,
                        IsRequired = true,
                        AttributeControlTypeId = 1,
                        DisplayOrder = 0
                    });

                    dbContext.SaveChanges();

                    //Add attribute mapping value
                    foreach (var attributeValue in item.Select(e => e.Maat).ToList())
                    {
                        if (String.IsNullOrEmpty(attributeValue))
                            continue;

                        dbContext.ProductAttributeValues.Add(new ProductAttributeValue()
                        {
                            ProductAttributeMappingId = mapping.Id,
                            AttributeValueTypeId = 0,
                            AssociatedProductId = 0,
                            Name = attributeValue,
                            PriceAdjustment = 0,
                            WeightAdjustment = 0,
                            Cost = 0,
                            Quantity = 1,
                            IsPreSelected = false,
                            DisplayOrder = 0,
                            PictureId = 0
                        });
                    }

                    dbContext.SaveChanges();

                    count++;
                    Console.WriteLine("Progress: " + count + "/" + articles);
                }
            }
        }
    }
}
